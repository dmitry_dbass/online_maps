/*
    ����� ���������/��������������� ����� - ����������� ���������� ������ � �������� � *.zmp
    ��������� ����� SASPlanet, � ��������� ��������� ������������ URL �����. ������, ���
    GetZ � ���� �������� - ��� ������� ���������� ����, ������� �������� ��������� � 1, � �� � 0. � ���,
    � � ���������� ���� ���� ����� ���������� � ����. ������� � �������� ����� GetZ-1

    ������ �������� Google - � tileUrlGoogle()

    ����� �������� ����� �������� - ������� ��������� ������������ URL �� �������� � ������� tileUrl... � ������ ����� �������
    � ������� vector<MapSource> map_src. ���� EPSG-��� (��� ������� ���������) ����������, �� ������ ����� ��� EPSG3785 (Web Mercator),
    ������ ��� EPSG3395 - ���������� �� ���������� - �������� ������ ������. �� ����� �������� - ����� ������� ����� ����� ������ ��
    ������� � Mapkit-Assistant
*/


#include <iostream>
#include <sstream>
#include <fstream>
#include <memory>
#include <vector>
#include <map>
#include <algorithm>
#include <cstring>
#include <cmath>

using namespace std;

enum MapType
{
	Sat=0,
	Topo,
	Hybrid,
	Vector
};


struct LLPoint
{
    LLPoint(unsigned x, unsigned y, unsigned zoom)
    {
        auto res = tile2LL(x,y,zoom);
        lon = res.first;
        lat = res.second;
    }
    double lon;
    double lat;

    pair<double,double> tile2LL(unsigned x, unsigned y, unsigned z)
    {
        double lon= x / (double)(1 << z) * 360.0 - 180;
        double n = M_PI - 2.0 * M_PI * y / (double)(1 << z);
        double lat = 180.0 / M_PI * atan(0.5 * (exp(n) - exp(-n)));
        return {lon, lat};
    }

};


int google_version{908};

unsigned long long random(int max)
{
    return static_cast<unsigned long long>(rand() % max);
}


struct MapSource
{
    using UrlFn = string (*)(int x, int y, int zoom);
    MapSource(const string name,
              const vector<unsigned> zoom,
              const MapType type_name,
              const string crs_name,
			  UrlFn fn,
			  const string cache_name,
              const string content_type,
              const int max_download_streams,
              const string alias) :
        name(name),
        zoom_level(zoom),
        type(type_name),
        crs(crs_name),
        tileUrl(fn),
        cache_name(cache_name),
        content_type(content_type),
        max_download_streams(max_download_streams),
        alias(alias)
        {}
    /// ��� ��������� ��� ��������� ���� - ������ Google, Yandex
    const string name;
    /// available zoom levels
    const std::vector<unsigned> zoom_level;
    const MapType type;
    const string crs;
    UrlFn tileUrl;
	const string cache_name;
    const string content_type;
    const int max_download_streams;
    const string alias; // ������������ ��� ��� readme.txt
};


string tileUrlGoogle(int x, int y, int zoom)
{
    char url[256];
    snprintf(url, sizeof(url),
             "https://khms%i.google.com/kh/v=%i&src=app&x=%i&y=%i&z=%i",
             random(4),
             google_version,
             x,
             y,
             zoom);
    return url;
}


string tileUrlYandexSat(int x, int y, int zoom)
{
    char url[256];
    snprintf(url, sizeof(url),
             "https://core-sat.maps.yandex.net/tiles?l=sat&x=%i&y=%i&z=%i&scale=1&lang=ru_RU",
             x,
             y,
             zoom);
    return url;
}

string tileUrlYandexTopo(int x, int y, int zoom)
{
    char url[256];
    snprintf(url, sizeof(url),
             "https://core-renderer-tiles.maps.yandex.net/tiles?l=map&x=%i&y=%i&z=%i&scale=1",
             x,
             y,
             zoom);
    return url;
}

string tileUrlMMB(int x, int y, int zoom)
{
    char url[256];

    snprintf(url, sizeof(url),
             //"http://redcross.msk.ru/files/maps/mmb/z%i/%i/%i.png", zoom, y, x);
             "https://slazav.xyz/tiles/podm/%i-%i-%i.png", x,y,zoom);
    return url;
}


string tileUrlOSM(int x, int y, int zoom)
{
    char url[256];

    snprintf(url, sizeof(url),
             "https://tile.openstreetmap.org/%i/%i/%i.png",
             zoom,
             x,
             y);

    return url;
}

string tileUrlGGCExtremum(int x, int y, int zoom)
{
    char url[256];

    snprintf(url, sizeof(url),
             "http://map.extremum.org/maps875/genshtab250m/z%i/%i/%i.jpg",
             zoom,
             y,
             x);

    return url;
}
///91.237.82.95:8088
string tileUrlGGC250(int x, int y, int zoom)
{
    char url[160];
    const char* base = (random(2) == 0) ? "https://maps.melda.ru/pub/ggc/250m.png/z%i/%i/x%i/%i/y%i.png" : "http://map.zeskmi.ru/ggc/250m.png/z%i/%i/x%i/%i/y%i.png";
    snprintf(url, sizeof(url),
             base,
             zoom+1,
             x/1024,
             x,
             y/1024,
             y);

    return url;
}


string tileUrlGGC250Nakarte(int x, int y, int zoom)
{
    char url[256];

    snprintf(url, sizeof(url),
			"https://%c.tiles.nakarte.me/ggc250/%i/%i/%i", 'a' + random(4), zoom, x, (1<<zoom)-1-y);
    return url;
}

string tileUrlGGCRedCross(int x, int y, int zoom)
{
    char url[160];
    snprintf(url, sizeof(url),
             "http://redcross.msk.ru/files/maps/ggc250/z%i/%i/%i.png",
             zoom,
             y,
             x);

    return url;
}

/*
string tileUrlGGC(int x, int y, int zoom)
{
    string base_url = random(2)==1 ?
                "http://91.237.82.95:8088/pub/ggc/" :
                "http://maps.melda.ru/pub/ggc/";

    if(zoom>=15) base_url.append("250m.png");
    if(zoom==14) base_url.append("500m.png");
    if(zoom==13) base_url.append("1km.png");

    char url[256];

    snprintf(url, sizeof(url),
             "/z%i/%i/x%i/%i/y%i.png",
             zoom+1,
             x/1024,
             x,
             y/1024,
             y);

    return base_url + url;
}
*/

string tileUrlBing(int x, int y, int zoom)
{
    int osX, osY, prX, prY;
    string res;
    osX=static_cast<int>(1)<<(zoom-1);
    osY=static_cast<int>(1)<<(zoom-1);
    prX=osX; prY=osY;
    for (int i=2; i<=(zoom+1); i++)
    {
        prX=prX / 2;
        prY=prY / 2;
        if (x<osX)
        {
            osX=osX-prX;
            if (y<osY)  {
                osY=osY-prY;
                res.push_back('0');
            }
            else {
                osY=osY+prY;
                res.push_back('2');
            }
        }
        else {
            osX=osX+prX;
            if (y<osY) {
                osY=osY-prY;
                res.push_back('1');
            }
            else {
                osY=osY+prY;
                res.push_back('3');
            }
        }
    }
    auto url=string("http://ecn.t0.tiles.virtualearth.net/tiles/a");
    url.append(res);
    url.append(".jpeg?g=0");
    return url;
}

string tileUrlArcGIS(int x, int y, int zoom)
{
    char url[160];

    snprintf(url, sizeof(url),
             "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/%i/%i/%i",
             zoom,
             y,
             x);

    return url;
}
string tileUrlWiki(int x, int y, int zoom)
{
    char url[256];

    LLPoint p1(x,y,zoom);
    LLPoint p2(x+1,y+1,zoom);

    snprintf(url, sizeof(url),
                     "http://wikimapia.org/d?lng=1&BBOX=%.7f,%.7f,%.7f,%.7f", p1.lon, p2.lat, p2.lon, p1.lat);
    return url;
}

string tileUrlMapbox(int x, int y, int zoom)
{
    char url[256];

    snprintf(url, sizeof(url),
             "https://api.tiles.mapbox.com/v4/mapbox.satellite/%i/%i/%i.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw", zoom, x, y);
    return url;
}

string tileFacebook(int x, int y, int zoom)
{
    char url[512];

    snprintf(url, sizeof(url),
             "https://www.facebook.com/maps/ml_roads?theme=ml_road_vector&collaborator=fbid&token=ASZUVdYpCkd3M6ZrzjXdQzHulqRMnxdlkeBJWEKOeTUoY_Gwm9fuEd2YObLrClgDB_xfavizBsh0oDfTWTF7Zb4C&hash=ASYM8LPNy8k1XoJiI7A&result_type=satellite_raster_tile&materialize=true&x=%i&y=%i&z=%i", x, y, zoom);
    return url;
}

string tileUrlNokia(int x, int y, int zoom)
{
    char url[256];

    snprintf(url, sizeof(url),
             "https://%i.aerial.maps.api.here.com/maptile/2.1/maptile/newest/satellite.day/%i/%i/%i",
             1+random(4),
             zoom,
             x,
             y);

    return string(url) + "/256/png8?app_id=xWVIueSv6JL0aJ5xqTxb&app_code=djPZyynKsbTjIUDOBcHZ2g&lg=rus&ppi=72&pview=DEF";
}



vector<MapSource> map_src {
    {"Google",{14,15,16,17},Sat,"EPSG3785",tileUrlGoogle,"sat","jpg",4,"Google"},
    {"Yandex",{14,15,16,17},Sat,"EPSG3395",tileUrlYandexSat,"yasat","jpg",4,"Yandex"},
    {"Bing",{14,15,16,17},Sat,"EPSG3785", tileUrlBing,"vesat","jpg",4,"Bing"},
    {"ArcGIS",{14,15,16,17},Sat,"EPSG3785",tileUrlArcGIS,"ArcGIS.Imagery","jpg",4,"ArcGIS"},
    {"Nokia",{14,15,16,17},Sat,"EPSG3785",tileUrlNokia,"here.com.satellite.jpg","jpg",4,"Nokia"},
    {"Mapbox",{14,15,16,17},Sat,"EPSG3785", tileUrlMapbox,"Mapbox_Sat","jpg",4,"Mapbox"},
    {"Facebook",{14,15,16,17},Sat,"EPSG3785", tileFacebook,"Maxar_via_Facebook","jpg",4,"Facebook"},

    {"GGC250",{14,15},Topo,"EPSG3785",tileUrlGGC250,"genshtab250m","png",4,"�����������"},
    {"GGCNak",{14,15},Topo,"EPSG3785",tileUrlGGC250Nakarte,"GGC.250m_nakarte","png",4,"�����������"},
    {"GGCRC",{14,15},Topo,"EPSG3785",tileUrlGGCRedCross,"ggc250RC","png",4,"�����������"},
    //{"GGC",{12,13,14,15},Topo,"EPSG3785", tileUrlGGC,"ggc_all"},
    {"GGCEx",{12,13,14},Topo,"EPSG3785",tileUrlGGCExtremum,"genshtab250mex","jpg",4,"�����������"},
    {"OSM",{14,15},Topo,"EPSG3785",tileUrlOSM,"osm_CycleMap","png",4,"OSM"},
    {"MMB",{14},Topo,"EPSG3785",tileUrlMMB,"wladich","png",4,"���"},
    {"Yandex",{14,15,16,17},Topo,"EPSG3395",tileUrlYandexTopo,"yamapng","png",4,"Yandex"},

    {"Wiki",{15},Topo,"EPSG3785",tileUrlWiki,"Wiki","kml",1,"Wiki"},
    
    
};

vector<string> result;
map<string, MapType> type_table{
	{"TOPO", MapType::Topo},
	{"SAT", MapType::Sat},
	{"HYB", MapType::Hybrid},
	{"VEC", MapType::Vector}
};

map<MapType, string> name_of_type{
	{MapType::Topo,"TOPO"},
	{MapType::Sat,"SAT"},
	{MapType::Hybrid,"HYB"},
	{MapType::Vector,"VEC"}
};

using MapSourceIterator = decltype (map_src)::const_iterator;


pair<string, MapSourceIterator> findMap(string name, string type_name, int zoom)
{
    MapType map_type;
	if(type_table.find(type_name) == type_table.cend())
		return {"ERR Invalid map type", MapSourceIterator()};
	map_type = type_table.at(type_name);

    auto msrc = find_if(map_src.cbegin(), map_src.cend(),
                        [map_type, &name](decltype (map_src)::const_reference val)->bool{
        if( val.name == name  && val.type == map_type)
            return true;
        return false;
    });

    if(msrc==map_src.cend())
        return {"ERR No such map",MapSourceIterator()};

	if(zoom!=-1)
	{
		if(zoom > *max_element(msrc->zoom_level.cbegin(), msrc->zoom_level.cend()))
			return {"ERR Zoom too big",MapSourceIterator()};
	}
    return {string(),msrc};

}

string tileUrl(const MapSource& msrc, unsigned x, unsigned y, int zoom)
{

    if( x > 1<<zoom || y> 1<<zoom)
        return "ERR x/y range error";

    return msrc.tileUrl(x,y,zoom);
}


int main(int argc, char** argv)
{

    array<char,256> input;
    result.reserve(4096);
    for(auto& r: result)
        r.reserve(256);
    while(!cin.getline(input.data(),sizeof(input)).eof())
    {
        string in(input.data());
        string request;
        istringstream ss(in);
        ss >> request;

        if(request == "LIST")
        {
            for(auto& m : map_src)
            {
                cout << m.name << ' ' << name_of_type.at(m.type) << ' ';
                cout << m.zoom_level.size() << ' ';
                for (auto z: m.zoom_level)
                    cout << z << ' ';
                cout << m.crs << ' ' << m.cache_name << ' ' <<  m.content_type << ' ' <<
                    m.max_download_streams <<  ' ' << m.alias << '\n';
            }
            cout << endl;
        }

        if(request == "TILE")
        {
            ss.exceptions(ios_base::failbit);

            string name, type;
            unsigned x, y, zoom;

            ss.exceptions(ios_base::failbit);
            try {
                ss >> name >> type >> x >> y >> zoom;
            } catch (ios_base::failure&) {
                cout <<  "ERR Parameter extraction error" << endl;
                continue;
            }

            auto msrc = findMap(name, type, zoom);
            if(!msrc.first.empty())
                cout << msrc.first << endl;
            else
                cout << tileUrl(*(msrc.second),x,y,zoom) << endl;
        }
		

        if(request == "TILE_REGION")
        {
            string name, type;
            unsigned x1, x2, y1, y2, zoom;

            result.clear();
            ss.exceptions(ios_base::failbit);
            try {
                ss >> name >> type >> x1 >> y1 >>x2 >> y2 >> zoom;
            } catch (ios_base::failure&) {
                cout <<  "ERR Parameter extraction error" << endl;
                continue;
            }

            if (x1>x2 || y1>y2)
            {
                cout << "ERR x2<x1 or y2<y1" << endl;
                continue;
            }

            auto msrc = findMap(name, type, zoom);
            if(!msrc.first.empty())
                cout << msrc.first << '\n';
            else
            {
                for(auto x=x1; x<=x2; x++)
                {
                    bool error=false;
                    for(auto y=y1; y<=y2; y++)
                    {
                        auto res=tileUrl(*(msrc.second),x,y,zoom);
                        if(!strncmp(res.c_str(),"ERR",3))
                        {
                            cout << res << '\n';
                            error=true;
                            break;
                        }
                        else
                        {
                            string r = to_string(x) + ' ' + to_string(y) + ' ' + res;
                            result.push_back(std::move(r));
                        }
                    }
                    if(error)
                        break;
                }
            }
            for(auto& r: result)
                cout << r << '\n';
            cout.flush();
        }

        if(request == "CACHE")
        {
            ss.exceptions(ios_base::failbit);

            string name, type;
			
            ss.exceptions(ios_base::failbit);
            try {
                ss >> name >> type;
            } catch (ios_base::failure&) {
                cout <<  "ERR Parameter extraction error" << endl;
                continue;
            }

            auto msrc = findMap(name, type, -1);
            if(!msrc.first.empty())
                cout << msrc.first << endl;
            else
                cout << (msrc.second)->cache_name << endl;
        }
        
        if(request == "GOOGLE_VERSION")
        {
            ss.exceptions(ios_base::failbit);

            string sver;
            int ver;
			
            ss.exceptions(ios_base::failbit);
            try {
                ss >> sver;
                google_version = stol(sver);
            } catch (ios_base::failure&) {
                cout <<  "ERR Parameter extraction error" << endl;
                continue;
            }
            catch (std::logic_error)
            {
                cout <<  "ERR Wrong number" << endl;
                continue;
            }
                

        }
        
        input.fill(0);
    }
}
